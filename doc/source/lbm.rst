Latent Block Model
------------------
.. image:: lbm.png
        :scale: 50
        :align: center
        :alt: lbm

The `LBM` class encapsulates the Latent Block Model and its random initialisation procedure.
Its use is similar to that of the `SBM` class and we refer the reader to the previous section for more details.

.. autoclass:: sparsebm.LBM
   :members:

   .. automethod:: __init__
