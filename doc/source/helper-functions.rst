Helper functions
================

.. toctree::
   :maxdepth: 2

   graph-gen-sbm
   graph-gen-lbm
   cari
