********
SparseBM
********
.. toctree::
   :maxdepth: 2
   :numbered:
   :caption: Table of Contents

   ./getting-started.rst
   ./class-description.rst
   ./helper-functions.rst
   ./cli-sparsebm.rst
   ./examples.rst
   ./experiments.rst


* :ref:`genindex`
